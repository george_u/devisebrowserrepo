#include "DialogConnectToServer.h"


DialogConnectToServer::DialogConnectToServer(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::DialogConnectToServer)
{
    ui->setupUi(this);
}

DialogConnectToServer::~DialogConnectToServer()
{
    delete ui;
}

QString DialogConnectToServer::getIp() {
    return ui->lineEditIp->text();
}

QString DialogConnectToServer::getPort() {
    return ui->lineEditPort->text();
}