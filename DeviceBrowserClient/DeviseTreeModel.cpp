#include "DeviseTreeModel.h"

DeviceTreeItem::DeviceTreeItem(const QVariant& data, std::shared_ptr<DeviceShared> devise, QPointer< DeviceTreeItem> parent)
    : itemData(data),
    parentItem(parent),
    devisePtr(devise)
{
    if(parentItem)
        parentItem->childItems.push_back(this);
}

DeviceTreeItem::DeviceTreeItem(const QVariant& data, QPointer< DeviceTreeItem> parent)
    : itemData(data),
    parentItem(parent),
    devisePtr(nullptr)
{
    if (parentItem)
        parentItem->childItems.push_back(this);
}

DeviceTreeItem::~DeviceTreeItem()
{
    //qDeleteAll(childItems); // this generates dangling pointers
}

DeviceTreeItem* DeviceTreeItem::parent()
{
    return parentItem;
}

// child() returns a specific child from the internal list of children
DeviceTreeItem* DeviceTreeItem::child(int number)
{
    if (number < 0 || number >= childItems.size())
        return nullptr;
    return childItems.at(number);
}

int DeviceTreeItem::childCount() const
{
    return childItems.count();
}

// return this item's number in its parent's list of children
int DeviceTreeItem::childNumber() const
{
    if (parentItem)
        return parentItem->childItems.indexOf(const_cast<DeviceTreeItem*>(this));
    return 0;
}

QVariant DeviceTreeItem::data() const
{
    return itemData;
}

QVariant DeviceTreeItem::icon() const
{
    return itemIcon;
}


bool DeviceTreeItem::setData(int column, const QVariant& value)
{
    itemData = value;
    return true;
}

bool DeviceTreeItem::setIcon(QIcon icon) {
    itemIcon = icon;
    return true;
}

bool DeviceTreeItem::removeChildren(int position, int count)
{
    if (position < 0 || position + count > childItems.size())
        return false;

    for (int row = 0; row < count; ++row)
        delete childItems.takeAt(position);

    return true;
}

std::shared_ptr<DeviceShared> DeviceTreeItem::device() {
    return devisePtr;
}

bool DeviceTreeItem::setDevice(std::shared_ptr<DeviceShared> devise) {
    devisePtr = devise;
    return true;
}




DeviceTreeModel::DeviceTreeModel(QObject* parent)
    : QAbstractItemModel(parent)
{}

DeviceTreeModel::~DeviceTreeModel()
{
    delete rootItem;
}

DeviceTreeItem* DeviceTreeModel::getItem(const QModelIndex& index) const
{
    if (index.isValid()) {
        DeviceTreeItem* item = static_cast<DeviceTreeItem*>(index.internalPointer());
        if (item)
            return item;
    }
    return rootItem;
}

int DeviceTreeModel::rowCount(const QModelIndex& parent) const
{
    if (parent.isValid() && parent.column() > 0)
        return 0;

    const DeviceTreeItem* parentItem = getItem(parent);

    return parentItem ? parentItem->childCount() : 0;
}

int DeviceTreeModel::columnCount(const QModelIndex& parent) const
{
    return 1;
}

Qt::ItemFlags DeviceTreeModel::flags(const QModelIndex& index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return QAbstractItemModel::flags(index);
}

QModelIndex DeviceTreeModel::index(int row, int column, const QModelIndex& parent) const
{
    if (parent.isValid() && parent.column() != 0) // only one column in a tree
        return QModelIndex(); // return invalid index
    DeviceTreeItem* parentItem = getItem(parent);
    if (!parentItem)
        return QModelIndex();// return invalid index

    DeviceTreeItem* childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    return QModelIndex();
}

QModelIndex DeviceTreeModel::parent(const QModelIndex& index) const
{
    if (!index.isValid())
        return QModelIndex();

    DeviceTreeItem* childItem = getItem(index);
    DeviceTreeItem* parentItem = childItem ? childItem->parent() : nullptr;

    if (parentItem == rootItem || !parentItem)
        return QModelIndex();

    return createIndex(parentItem->childNumber(), 0, parentItem);
}

QVariant DeviceTreeModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();
    DeviceTreeItem* item = static_cast<DeviceTreeItem*>(index.internalPointer());

    switch (role)
    {
    case Qt::DisplayRole:
        return item->data();
        break;
    case Qt::DecorationRole:
        return item->icon();
        break;
    default:
        return QVariant();
    }
}

QVariant DeviceTreeModel::headerData(int section, Qt::Orientation orientation,
    int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data();

    return QVariant();
}

bool DeviceTreeModel::setRootItem(QPointer< DeviceTreeItem> root) {
    rootItem = root;
    return true;
}

bool DeviceTreeModel::clear() {
    if (rootItem)
        delete rootItem;
    return true;
}

std::shared_ptr<DeviceShared> DeviceTreeModel::device(const QModelIndex& index) const
{
    if (!index.isValid())
        return nullptr;

    DeviceTreeItem* item = getItem(index);
    return item->device();
}