#pragma once

#include <list>
#include <memory>
#include <map>

#include "Server.h"
#include "Device.h"
#include "FileTransmission.h"

namespace Globals
{
	extern std::list<std::shared_ptr<Device>> deviceList;
	extern std::shared_ptr<DeviceInfoSet> DeviceInfo;
	extern std::wstring tempDirectory;
	extern bool isUpdated;
	extern std::string portNumber;
	extern std::list<std::shared_ptr<FileTransmission>> fileTransmissions;
	void updateDeviceList();
}
