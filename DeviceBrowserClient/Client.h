#pragma once

#include <QString>
#include <memory>
#include <list>

#include "../common/DeviceShared.h"
#include "../common/DriverShared.h"
#include "ServerConnection.h"

class Client
{
public:
	Client(
		QString ip,
		QString port
	);
	bool askAboutUpdates();
	std::list<std::shared_ptr<DeviceShared>> update(bool showHidden);
	std::shared_ptr<DriverShared> Client::driverInfo(std::shared_ptr<DeviceShared> device);
	bool isConnected() const;
	bool enableDevice(std::shared_ptr<DeviceShared> device);
	bool disableDevice(std::shared_ptr<DeviceShared> device);
	bool uninstallDevice(std::shared_ptr<DeviceShared> device);
	bool sendFile(QString fname);
	bool installInf(QString fname, std::shared_ptr<DeviceShared> device);
	~Client();
private:
	ServerConnection _connection;
	const int WAITING_TIMEOUT_VERY_SHORT = 100;
	const int WAITING_TIMEOUT_SHORT = 2000;
	const int WAITING_TIMEOUT_LONG = 12000;
};

