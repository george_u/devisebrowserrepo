#include "Device.h"
#include <Devpkey.h>
#include <sstream>

Device::Device(std::shared_ptr<DeviceInfoSet> devInfoPtr, SP_DEVINFO_DATA devInfoData, GUID guid, std::wstring name, std::wstring description, std::wstring manufacturer) :
	_devInfoSetPtr(devInfoPtr),
	guid(guid),
	name(name),
	description(description),
	manufacturer(manufacturer),
	isShown(false), isEnabled(false)
{
	instanceCode = devInfoData.DevInst;
	_devInfoData = std::unique_ptr<SP_DEVINFO_DATA>(new SP_DEVINFO_DATA(devInfoData));
}

Device::~Device() {
	SetupDiDeleteDeviceInfo(_devInfoSetPtr->devInfoSet(), _devInfoData.get());
}

HDEVINFO Device::devInfoSet() const
{
	if (_devInfoSetPtr)
		return _devInfoSetPtr->devInfoSet();
	return INVALID_HANDLE_VALUE;
}

std::list<std::shared_ptr<Driver>> Device::getDriverList() const {
	bool success;
	std::list<std::shared_ptr<Driver>> result;

	success = SetupDiBuildDriverInfoList(_devInfoSetPtr->devInfoSet(), _devInfoData.get(), SPDIT_COMPATDRIVER);// necessary before calling SetupDiEnumDriverInfo

	if (!success)
		return result;// return empty because of the error

	// TODO

	SP_DRVINFO_DATA drvInfo;
	drvInfo.cbSize = sizeof(SP_DRVINFO_DATA);

	for (int i = 0; i < 10; i++) {
		if (!SetupDiEnumDriverInfo(_devInfoSetPtr->devInfoSet(), _devInfoData.get(), SPDIT_COMPATDRIVER, i, &drvInfo)) {// no more drivers for the current device
			break; // no drivers left
		}
		else {
			std::shared_ptr<Driver> drv(new Driver(
				std::wstring(drvInfo.Description),
				std::wstring(drvInfo.ProviderName),
				std::wstring(drvInfo.MfgName),
				std::to_wstring(drvInfo.DriverVersion)
			));
			result.push_back(drv);
		}
	}

	SetupDiDestroyDriverInfoList(_devInfoSetPtr->devInfoSet(), _devInfoData.get(), SPDIT_COMPATDRIVER);
	return result;
}

bool Device::enable() {
	return CR_SUCCESS == CM_Enable_DevNode(
		_devInfoData->DevInst,
		NULL // flags
	);
}

bool Device::disable() {
	return CR_SUCCESS == CM_Disable_DevNode(
		_devInfoData->DevInst,
		NULL // flags
	);
}

bool Device::uninstall() {
	// DIF_REMOVE function is to uninstall the device
	return SetupDiCallClassInstaller(
		DIF_REMOVE,//DI_FUNCTION      InstallFunction,
		_devInfoSetPtr->devInfoSet(),//HDEVINFO         DeviceInfoSet,
		_devInfoData.get()//PSP_DEVINFO_DATA DeviceInfoData
	);
	// it will return true if worked successfully
}

std::list<std::wstring> Device::getHardwareId() {
	std::list<std::wstring> result;
	const DWORD bufsize = 1024;
	int errCode;
	bool success = false;
	wchar_t nameStr[bufsize * sizeof(wchar_t)];
	DWORD nameStrSize;

	DEVPROPTYPE strListPropertyType = DEVPROP_TYPE_STRING_LIST; // the API wants it as a pointer

	success = SetupDiGetDeviceProperty(
		_devInfoSetPtr->devInfoSet(),
		_devInfoData.get(),
		&DEVPKEY_Device_HardwareIds, // hope to get the name
		&strListPropertyType,
		(PBYTE)&nameStr,
		bufsize,
		&nameStrSize, // RequiredSize, not interesting
		0 //This parameter must be set to zero
	);
	// string1\0string2\0string3 -> vector
	std::wstringstream sstr;
	for (int i = 0; i < nameStrSize-1; i++) {
		
		if (nameStr[i] != '\0') {
			sstr << nameStr[i];
		}
		else {
			result.push_back(sstr.str());
			sstr = std::wstringstream();
		}
	}
	

	return result;

	//if (!success)
	//	return L"";
	//return std::wstring(nameStr, nameStrSize);
}