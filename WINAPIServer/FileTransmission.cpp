#include "FileTransmission.h"

#include "Globals.h"
#include <sstream>
#include <Windows.h>


FileTransmission::FileTransmission(std::string fname, int fSize)
    :
    _fname(fname), _isFinished(false), _isValid(true), _size(fSize), _bytesRecieved(0)
{
    _handle = CreateFile(
        (Globals::tempDirectory + L"/" + StrToWStr(fname)).c_str(),                // name of the file
        FILE_APPEND_DATA,          // open for writing
        0,                      // do not share
        NULL,                   // default security
        CREATE_NEW,             // create new file only
        FILE_ATTRIBUTE_NORMAL,  // normal file
        NULL);                  // no attr. template

    if (_handle == INVALID_HANDLE_VALUE)
    {
        _isValid = false;
    }
}

std::wstring FileTransmission::StrToWStr(std::string arg)
{
    std::wstringstream stream;
    for (char c : arg) {
        stream << c;
    }
    return stream.str();
}

std::wstring FileTransmission::currentDirectory()
{
    WCHAR buffer[MAX_PATH];

    GetCurrentDirectory(
        MAX_PATH,//DWORD  nBufferLength,
        buffer//LPTSTR lpBuffer
    );

    return std::wstring(buffer);
}

std::string FileTransmission::getFname() const
{
    return _fname;
}

bool FileTransmission::addBytes(const char* data, int dataLength)
{
    DWORD dataWritten = 0;
    bool success = WriteFile(
        _handle,           // open file handle
        data,      // start of data to write
        dataLength,  // number of bytes to write
        &dataWritten, // number of bytes that were written
        NULL);            // no overlapped structure
    if (success)
        _bytesRecieved += dataWritten;
    if (_bytesRecieved == _size)
        finish();
    return success;
}

bool FileTransmission::isFinished() const
{
    return _isFinished;
}

bool FileTransmission::isValid() const
{
    return _isValid;
}


void FileTransmission::finish()
{
    if (_isValid) {
        CloseHandle(_handle);
        _isFinished = true;
    }
        
}

void FileTransmission::cleanAll()
{
    // delete temporary files
    for (auto&& file : Globals::fileTransmissions) {
        if (file->isValid() && file->isFinished()) {
            std::wstring fullPath = Globals::tempDirectory + L"/" + StrToWStr(file->getFname());
            bool success = DeleteFile(fullPath.c_str());
        }
    }
}