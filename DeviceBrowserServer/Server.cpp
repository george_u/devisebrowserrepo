#pragma once
#include "Server.h"

#include <iostream>
#include <sstream>
#include <stdio.h>
#include <conio.h>

// ��� ���������� ������ freeaddrinfo � MinGW
// ���������: http://stackoverflow.com/a/20306451
#define _WIN32_WINNT 0x501

#pragma comment(lib, "Ws2_32.lib")

#include "Messages.h"


void Server::run() {
	_listen();
	if (_clientSocket != INVALID_SOCKET)
		_serve();

}

Server::Server(std::string port) : _clientSocket(INVALID_SOCKET), _port(port)
{
	int errCode;
	// Initialize Winsock
	errCode = WSAStartup(MAKEWORD(2, 2), &_wsaData);
	/* The MAKEWORD(2,2) parameter of WSAStartup makes a request for version 2.2 of Winsock on the system,
	and sets the passed version as the highest version of Windows Sockets support that the caller can use */
	if (errCode != NO_ERROR) {
		printf("WSAStartup failed: %d\n", errCode);
		return;
	}

	struct addrinfo* result = NULL, * ptr = NULL, hints;

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the local address and port to be used by the server
	errCode = getaddrinfo(NULL, _port.c_str(), &hints, &result);
	if (errCode != NO_ERROR) {
		printf("getaddrinfo failed: %d\n", errCode);
		return;
	}

	// save the result
	_addrinfo = *result;
}

Server::~Server() {
	// Once the bind function is called, the address information returned by the getaddrinfo function is no longer needed
	//freeaddrinfo(&_addrinfo); // causes an error
	WSACleanup(); // Cleanup winsock objects
}

void Server::_listen() {
	int errCode;
	// Create a SOCKET for the server to listen for client connections
	SOCKET ListenSocket = socket(_addrinfo.ai_family, _addrinfo.ai_socktype, _addrinfo.ai_protocol);

	if (ListenSocket == INVALID_SOCKET) {
		printf("Error at socket(): %ld\n", WSAGetLastError());
		freeaddrinfo(&_addrinfo);
		return;
	}

	// Setup the TCP listening socket
	errCode = bind(ListenSocket, _addrinfo.ai_addr, (int)_addrinfo.ai_addrlen);

	if (errCode == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		return;
	}

	// Start listening
	if (listen(ListenSocket, SOMAXCONN) == SOCKET_ERROR) {
		printf("Listen failed with error: %ld\n", WSAGetLastError());
		closesocket(ListenSocket);
		return;
	}

	//SOCKET ClientSocket;	// A SOCKET for accepting clients connections (now global)
	printf("The server is listening\nPress anything to stop...\n");

	while (!_kbhit()) {	// _kbhi() from conio.h will return TRUE if the user typed something 

		// Checking for pending connections
		timeval timeout;
		timeout.tv_sec = 1;
		timeout.tv_usec = 0;

		fd_set readySockets;
		FD_ZERO(&readySockets); // empty fd_set
		FD_SET(ListenSocket, &readySockets); // specify which socket we are interested in

		errCode = select(
			NULL, // reserved
			&readySockets,
			NULL,
			NULL,
			&timeout
		);	// select() will mark in fd_set which sockets are ready to recv/accept
		if (errCode == SOCKET_ERROR) {
			printf("failed select, error: %d\n", WSAGetLastError());
			closesocket(ListenSocket);
			return;
		}
		bool isReady = FD_ISSET(ListenSocket, &readySockets); // will be true if the listening socket can accept without blocking

		if (isReady) {
			// Accept a client socket
			_clientSocket = accept(ListenSocket, NULL, NULL);
			if (_clientSocket == INVALID_SOCKET) {
				printf("accept failed: %d\n", WSAGetLastError());
				closesocket(ListenSocket);
			}
			else {
				printf("a client connected\n");
			}
			closesocket(ListenSocket);
			return; // now we can work with the client
		}
	}
	closesocket(ListenSocket);
}


void Server::_serve() {
	int errCode;
	bool success;

	printf("The server is working\nPress anything to stop...\n");

	while (!_kbhit()) {	// _kbhi() from conio.h will return TRUE if the user typed something 
		// Checking for data ready
		timeval timeout;
		timeout.tv_sec = 0;
		timeout.tv_usec = 100000; // 0.1 sec

		fd_set readySockets;
		FD_ZERO(&readySockets); // empty fd_set
		FD_SET(_clientSocket, &readySockets); // specify which socket we are interested in

		errCode = select(
			NULL, // reserved
			&readySockets,
			NULL,
			NULL,
			&timeout
		);	// select() will mark in fd_set which sockets are ready to recv/accept
		if (errCode == SOCKET_ERROR) {
			printf("failed select, error: %d\n", WSAGetLastError());
			closesocket(_clientSocket);
			WSACleanup();
			return;
		}
		bool isReady = FD_ISSET(_clientSocket, &readySockets); // will be true if the socket have something to read

		if (isReady) {// there's something to read and respond
			//const int buflen = 1028*100; // 100 KB buffer

			//char buffer[buflen];
			// read the actual data
			int recievedBytes = recv(_clientSocket, _buffer, _buflen, 0);
			if (recievedBytes > 0) {
				std::stringstream data(std::string(_buffer, recievedBytes));

				// start processing the message
				std::string messageLine;
				if (std::getline(data, messageLine)) {// message line exists
					if (messageLine == "GIVE_DEVICE_LIST") {
						//  finding and sending the device list
						_send(Messages::takeDeviceList());
					}
					else if(messageLine == "GIVE_DEVICE_LIST_WITH_HIDDEN") {
						//  finding and sending the device list
						_send(Messages::takeDeviceListWithHidden());
					}
					else if (messageLine == "GIVE_DRIVER_PROPERTIES") {
						//  finding and sending the driver info for the device asked
						_send(Messages::takeDriverProperties(data));
					}
					printf("Message: %s\n", messageLine.c_str());
				}

			}
			else if (recievedBytes == 0) {
				// connection's closed
				printf("Connection's closed\n");
				break;
			}
			else{
				// connection's lost
				printf("recv() failed: %d\n", WSAGetLastError());
				printf("Connection's lost\n");
				break;
			}
			// bytes were recieved, time to read the message

		}
	}
	errCode = shutdown(_clientSocket, SD_SEND); // the work is done, saying goodbye
	closesocket(_clientSocket);
}

bool Server::_send(std::string data) {
	int errCode = send(_clientSocket, data.c_str(), data.size(), 0);
	if (errCode == SOCKET_ERROR) {
		printf("send failed: %d\n", WSAGetLastError());
		return false;
	}
	return true;
}