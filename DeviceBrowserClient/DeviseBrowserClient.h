#pragma once

#include "ui_DeviseBrowserClient.h"
#include <guiddef.h>
#include <QtWidgets/QMainWindow>
#include <QPointer>
#include <QTimer>
#include <list>
#include <memory>

#include "Client.h"
#include "DeviseCategory.h"
#include "../common/DeviseDriver.h"
#include "RequestToServer.h"
#include "DeviseTreeModel.h"

#pragma comment(lib, "user32.lib") // Visual studio links this automatically, but i was needed to add this library because otherwise Qt has a linker error with DestroyIcon()

class DeviseDriver;
class DeviceCategory;

class DeviseBrowserClient : public QMainWindow
{
    Q_OBJECT

public:
    DeviseBrowserClient(QWidget *parent = Q_NULLPTR);
public slots:
	void connectServer();
	void updateServer();
	void sendFiles();
	void updateServerWithHidden();
	void showHiddenClicked();
	void onTimerTicks();
	void DeviseBrowserClient::itemClicked(QModelIndex index);
	void DeviseBrowserClient::itemClickedEnable(QModelIndex index);
	void DeviseBrowserClient::itemClickedDisable(QModelIndex index);
	void DeviseBrowserClient::itemClickedUninstall(QModelIndex index);
	void DeviseBrowserClient::itemClickedInstall(QModelIndex index);
	void DeviseBrowserClient::onCustomContextMenu(const QPoint& point);
private:
	void DeviseBrowserClient::_formTree(std::list<std::shared_ptr<DeviceShared>>& list);
	std::list<DeviseDriver> _devices;
	std::list<DeviceCategory> _cats;
	DeviceTreeModel _treeModel;
	bool _showHidden;
    Ui::DeviseBrowserClientClass ui;
	std::shared_ptr<Client> _client;
	QPointer<QTimer> _timer;
};