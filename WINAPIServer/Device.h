#pragma once

#include <guiddef.h>
#include <string>
#include <list>
#include <memory>

#include "DeviceInfoSet.h"
#include "Driver.h"

class Device {
public:
	GUID guid;
	long instanceCode;
	std::wstring name, description, manufacturer;
	Device(
		std::shared_ptr<DeviceInfoSet>,
		SP_DEVINFO_DATA,
		GUID guid,
		std::wstring name,
		std::wstring description,
		std::wstring manufacturer
	);
	~Device();
	HDEVINFO devInfoSet() const;
	std::list<std::shared_ptr<Driver>> getDriverList() const;
	bool isShown, isEnabled;
	bool enable();
	bool disable();
	bool uninstall();
	std::list<std::wstring> getHardwareId();
private:
	//const DeviceInfoSet* _devInfoSetPtr;
	std::shared_ptr<DeviceInfoSet> _devInfoSetPtr;
	std::unique_ptr<SP_DEVINFO_DATA> _devInfoData;
};

