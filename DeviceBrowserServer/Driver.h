#pragma once

//#include <guiddef.h>
#include <string>

// Represents parameters of a devise and its driver 
class Driver {
public:
	//GUID guid;
	std::wstring description, providerName, mfgName, version;
	Driver(
		//GUID guid,
		std::wstring description,
		std::wstring providerName,
		std::wstring mfgName,
		std::wstring version
	);
};

