#pragma once

#include <Windows.h> // SetupAPI depends on it
#include <SetupAPI.h>
#pragma comment(lib, "Setupapi.lib")

class DeviceInfoSet
{
public:
	DeviceInfoSet();
	~DeviceInfoSet();
	HDEVINFO devInfoSet() const;
private:
	HDEVINFO _devInfoSet;
};

