/*
	!This file is shared between the client and server
*/
#pragma once

#include <string>
#include <guiddef.h>

// Represents parameters of a devise and its driver 
class DriverShared {
public:
	std::wstring description, providerName, mfgName, version;
	DriverShared(
		std::wstring description,
		std::wstring providerName,
		std::wstring mfgName,
		std::wstring version
	);
	DriverShared(std::stringstream& data);
	DriverShared(std::stringstream& data, bool& success);
	std::string stringify();

private:
	bool _parse(std::stringstream& data);
};
