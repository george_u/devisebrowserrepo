#pragma once

#include <QString>
#include <QTcpSocket>

class ServerConnection
{
public:
	ServerConnection(QString ip, qint16 port);
	bool isActive() const;
	bool tryRead(QByteArray& result, int timeout = 1000);
	void ServerConnection::send(const QByteArray& data);
	~ServerConnection();
private:
	QString _ip;
	qint16 _port;
	QTcpSocket _sock;
};

