#pragma once
#include <qdialog.h>


#include "ui_DialogConnectToServer.h"

class DialogConnectToServer :
    public QDialog
{
    Q_OBJECT
public:
    DialogConnectToServer(QWidget* parent);
    ~DialogConnectToServer();
    QString getIp();
    QString getPort();
private:
    Ui::DialogConnectToServer* ui;
};

