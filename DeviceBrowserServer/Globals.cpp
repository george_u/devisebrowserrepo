#include "Globals.h"

#include "GetAllDevices.h"

namespace Globals
{
	std::list<std::shared_ptr<Device>> deviceList;
	DeviceInfoSet DeviceInfo;

	void updateDeviceList() {
		// recreate
		DeviceInfo = DeviceInfoSet();
		deviceList.clear();
		
		// make the list
		deviceList = getDeviceList(&Globals::DeviceInfo);
	}
};
