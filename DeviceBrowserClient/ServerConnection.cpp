#include "ServerConnection.h"

bool ServerConnection::isActive() const
{
	return _sock.isValid() && _sock.state() == QAbstractSocket::ConnectedState;
}

ServerConnection::ServerConnection(QString ip, qint16 port) : _ip(ip), _port(port)
{
	_sock.connectToHost(ip, port);
	const int Timeout = 2 * 1000; // 2 sec timeout
	if (!_sock.waitForConnected(Timeout)) {	// blocking the thread by waiting for connection signal
		_sock.close();
	}
}

ServerConnection::~ServerConnection()
{
	if (_sock.isValid()) {
		if(_sock.state() == QAbstractSocket::ConnectedState)
			_sock.disconnect();
		_sock.close();
	}
}

bool ServerConnection::tryRead(QByteArray& result, int timeout)
{
	if (_sock.waitForReadyRead(timeout)) {	// waiting N milliseconds for data
		//while (_sock.bytesAvailable() > 0)// data is avaiable, so read
		//{
		//	result.append(_sock.readAll());
		//	//_sock.flush();
		//}
		result = _sock.readAll();
		return true;
	}
	// data is not avaiable
	return false;
}

void ServerConnection::send(const QByteArray& data)
{
	_sock.write(data);
}