#include "Client.h"

#include <sstream>
#include <QFile>
#include <QFileInfo>


Client::Client(QString ip, QString port) : _connection(ip, port.toInt()) {}

Client::~Client(){}

bool _successCheck(std::stringstream& resultStream);

std::list<std::shared_ptr<DeviceShared>> Client::update(bool showHidden) {
	std::list<std::shared_ptr<DeviceShared>> result;

	if(showHidden)
		_connection.send(QByteArray("GIVE_DEVICE_LIST_WITH_HIDDEN\n"));
	else
		_connection.send(QByteArray("GIVE_DEVICE_LIST\n"));
	
	QByteArray buffer;
	bool success = _connection.tryRead(buffer, WAITING_TIMEOUT_SHORT);

	if (success) {
		std::stringstream data(buffer.toStdString());
		std::string line;
		success &= (std::getline(data, line)) ? true : false;

		if (success && line == "TAKE_DEVICE_LIST") {
			
			bool parsingSuccess = true;
			while (parsingSuccess)
			{
				auto devShared = std::make_shared<DeviceShared>(data, parsingSuccess);
				if (!parsingSuccess)
					break;
				result.push_back(devShared);
			}
		}
	}
	return result;
}

bool Client::askAboutUpdates() {
	_connection.send(QByteArray("TELL_IF_UPDATED\n"));
	QByteArray buffer;
	bool success = _connection.tryRead(buffer, WAITING_TIMEOUT_VERY_SHORT); // only 100 ms waiting

	if (success) {
		std::stringstream data(buffer.toStdString());
		std::string line;
		success &= (std::getline(data, line)) ? true : false;

		if (success && line == "IS_UPDATED") {

			success &= (std::getline(data, line)) ? true : false;
			if (success && line == "YES") {
				return true;
			}
		}
	}
	return false;
}

std::shared_ptr<DriverShared> Client::driverInfo(std::shared_ptr<DeviceShared> device) {
	// encode the message (Header + device)
	std::stringstream stream;
	if (device == nullptr)
		return nullptr;
	stream << "GIVE_DRIVER_PROPERTIES\n";
	stream << device->stringify();

	_connection.send(QByteArray::fromStdString(stream.str()));
	QByteArray buffer;
	bool success = _connection.tryRead(buffer, WAITING_TIMEOUT_SHORT);

	if (success) {
		std::stringstream data(buffer.toStdString());
		std::string line;
		success &= (std::getline(data, line)) ? true : false;

		if (success && line == "TAKE_DRIVER_PROPERTIES") {

			bool parsingSuccess = true;
			auto drvShared = std::make_shared<DriverShared>(data, parsingSuccess);
			return drvShared;
		}
	}
	return nullptr;
}


bool Client::enableDevice(std::shared_ptr<DeviceShared> device) {
	// encode the message (Header + device)
	std::stringstream stream;
	if (device == nullptr)
		return false;
	stream << "ENABLE_DEVICE\n";
	stream << device->stringify();

	_connection.send(QByteArray::fromStdString(stream.str()));

	QByteArray buffer;
	bool success = _connection.tryRead(buffer, WAITING_TIMEOUT_SHORT); // 2 sec waiting

	if (success) {
		std::stringstream data(buffer.toStdString());
		std::string line;
		success &= (std::getline(data, line)) ? true : false;

		if (success && line == "SUCCESS") {
			return true;
		}
	}
	return false;
}
bool Client::disableDevice(std::shared_ptr<DeviceShared> device) {
	// encode the message (Header + device)
	std::stringstream stream;
	if (device == nullptr)
		return false;
	stream << "DISABLE_DEVICE\n";
	stream << device->stringify();

	_connection.send(QByteArray::fromStdString(stream.str()));

	QByteArray buffer;
	bool success = _connection.tryRead(buffer, WAITING_TIMEOUT_SHORT); // 2 sec waiting

	if (success) {
		std::stringstream data(buffer.toStdString());
		std::string line;
		success &= (std::getline(data, line)) ? true : false;

		if (success && line == "SUCCESS") {
			return true;
		}
	}
	return false;
}

bool Client::uninstallDevice(std::shared_ptr<DeviceShared> device) {
	// encode the message (Header + device)
	std::stringstream stream;
	if (device == nullptr)
		return false;
	stream << "UNINSTALL_DEVICE\n";
	stream << device->stringify();

	_connection.send(QByteArray::fromStdString(stream.str()));

	QByteArray buffer;
	bool success = _connection.tryRead(buffer, WAITING_TIMEOUT_SHORT); // 2 sec waiting

	if (success) {
		std::stringstream data(buffer.toStdString());
		std::string line;
		success &= (std::getline(data, line)) ? true : false;

		if (success && line == "SUCCESS") {
			return true;
		}
	}
	return false;
}

bool Client::isConnected() const {
	return _connection.isActive();
}

bool Client::sendFile(QString fname) {
	QFile file(fname);
	if (!file.open(QIODevice::ReadOnly))
		return false;
	QFileInfo info(file);
	QByteArray data = file.readAll();

	bool success;
	std::stringstream resultStream;
	// encode the init message (file name)
	std::stringstream stream;
	stream << "INIT_FILE\n";
	// File name
	stream << info.fileName().toStdString() << '\n';
	// File size
	stream << info.size() << '\n';

	_connection.send(QByteArray::fromStdString(stream.str()));
	QByteArray buffer;
	success = _connection.tryRead(buffer, WAITING_TIMEOUT_SHORT); // 2 sec waiting for result
	if (!success) { file.close(); return false; }
	// check 
	resultStream = std::stringstream(buffer.toStdString());
	if (!_successCheck(resultStream)) { file.close(); return false; }
	//sending the file
    const int MAX_PORTION = 1024;
    int p = 0, portion;
    while(p < data.size()){
        // send a portion
		portion = MAX_PORTION;// buffer.size() - p;
		if (portion + p > data.size())
			portion = data.size() - p;
        stream = std::stringstream();
        stream << "WRITE_FILE\n";
        stream << info.fileName().toStdString() << '\n';
        stream << portion << '\n'; // size of data
		const char* portionPtr = data.constData() + p;
        stream.write(portionPtr, portion);
        _connection.send(QByteArray::fromStdString(stream.str()));
        success = _connection.tryRead(buffer, 4000); // 4 sec waiting for result
        if (!success) { file.close(); return false; }
        // check
        resultStream = std::stringstream(buffer.toStdString());
        if (!_successCheck(resultStream)) { file.close(); return false; }
        p += portion;
    }
	////closing the file
	//stream = std::stringstream();
	//stream << "FINISH_FILE\n";
	//stream << info.fileName().toStdString() << '\n';
	//_connection.send(QByteArray::fromStdString(stream.str()));
	//success = _connection.tryRead(buffer, 2000); // 2 sec waiting for result
	//if (!success) { file.close(); return false; }
	//// check 
	//resultStream = std::stringstream(buffer.toStdString());
	//if (!_successCheck(resultStream)) { file.close(); return false; }
	
	// all done
	file.close();
	return true;
}

bool Client::installInf(QString fname, std::shared_ptr<DeviceShared> device) {
	bool success;
	std::stringstream resultStream;
	std::stringstream stream;
	// forming the message (header)
	stream = std::stringstream();
	stream << "INSTALL_INF\n";
	stream << fname.toStdString() << '\n';
	// encode the message (Device)
	if (device == nullptr)
        return false;
	stream << device->stringify();
	// sending
	_connection.send(QByteArray::fromStdString(stream.str()));
	// reading the result
	QByteArray buffer;
	success = _connection.tryRead(buffer, WAITING_TIMEOUT_LONG); // 12 sec waiting for result, it's a long process
	if (!success) { return false; }
	// check 
	resultStream = std::stringstream(buffer.toStdString());
	if (!_successCheck(resultStream)) { return false; }
	// all done
	return true;
}

bool _successCheck(std::stringstream& resultStream) {
	std::string line;
	return (std::getline(resultStream, line) && line == "SUCCESS");
}
