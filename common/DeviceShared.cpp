/*
	!This file is shared between the client and server
*/
#include "../common/DeviceShared.h"

#include <string>
#include <sstream>

DeviceShared::DeviceShared(
	GUID guid,
	long instanceCode,
	std::wstring name,
	std::wstring description,
	std::wstring manufacturer
) :
	guid(guid), name(name), description(description), manufacturer(manufacturer), instanceCode(instanceCode), isHidden(false), isEnabled(false) {}

DeviceShared::DeviceShared(
	GUID guid,
	long instanceCode,
	std::wstring name,
	std::wstring description,
	std::wstring manufacturer,
	bool hidden,
	bool enabled
) :
	guid(guid), name(name), description(description), manufacturer(manufacturer), instanceCode(instanceCode), isHidden(hidden), isEnabled(enabled){}

DeviceShared::DeviceShared(std::stringstream &data, bool& successful) : guid() {
	successful = _parse(data);
}

DeviceShared::DeviceShared(std::stringstream& data) : guid() {
	_parse(data);
}

bool DeviceShared::_parse(std::stringstream& data) {
	bool success = true;
	// read the guid
	std::string guidStr;
	success &= (std::getline(data, guidStr)) ? true : false;
	std::stringstream guidStream(guidStr);

	if (success) {
		guidStream >> guid.Data1 >> guid.Data2 >> guid.Data3;// >> chars;
		for (int i = 0; i < 8; i++) {
			unsigned int num;
			guidStream >> num;
			guid.Data4[i] = num;
		}
	}
	//read the instance code and hidden flag
	std::string instStr;
	success &= (std::getline(data, instStr)) ? true : false;
	std::stringstream instStream(instStr);
	instStream >> instanceCode;
	instStream >> std::boolalpha >> isHidden >> isEnabled;
	// read the others
	//read the pack size in bytes
	int packSize = 0;
	success &= (data >> packSize) ? true : false;
	// remove an empty line
	if (success)
		success &= (data.ignore(256, '\n')) ? true : false;

	char* pack = new char[packSize];
	data.read(pack, packSize);
	std::wstringstream packStream(std::wstring((const wchar_t*)pack, packSize / sizeof(wchar_t))); 
	// read the Unicode information
	if (success)
		success &= (std::getline(packStream, name)) ? true : false;
	if (success)
		success &= (std::getline(packStream, description)) ? true : false;
	if (success)
		success &= (std::getline(packStream, manufacturer)) ? true : false;
	// remove the last empty line
	if (success)
		success &= (data.ignore(256, '\n')) ? true : false;
	if (!success)
		name = L"!!!!error reading";
	delete []pack;
	return success;
}

std::string DeviceShared::stringify() {
	std::stringstream result; // using string stream to construct the responce efficiently

	// this code writes the devises to text to send through sockets
	result <<
			guid.Data1 << ' ' <<
			guid.Data2 << ' ' <<
			guid.Data3 << ' ';
	for (int i = 0; i < 8; i++)
		result << (unsigned int)guid.Data4[i] << ' ';
	result << '\n' << instanceCode << ' ' << std::boolalpha /* for "true" & "false" */ << isHidden  << ' ' << isEnabled;
	// pack the strings
	std::wstringstream pack; // using string stream to construct the responce efficiently
	
	pack << name.c_str() << L'\n';
	pack << description.c_str() << L'\n';
	pack << manufacturer.c_str() << L'\n';
	std::wstring packStr = pack.str();
	int packSize = packStr.size() * sizeof(wchar_t); // size in bytes

	result << '\n' << packSize << '\n';
	result.write((const char*)packStr.c_str(), packSize);
	result << '\n';

	return result.str();
}