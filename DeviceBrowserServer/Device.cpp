#include "Device.h"

Device::Device(const DeviceInfoSet* devInfoPtr, SP_DEVINFO_DATA devInfoData, GUID guid, std::wstring name, std::wstring description, std::wstring manufacturer) :
	_devInfoSetPtr(devInfoPtr),
	guid(guid),
	name(name),
	description(description),
	manufacturer(manufacturer),
	isShown(false)
{
	instanceCode = devInfoData.DevInst;
	_devInfoData = std::unique_ptr<SP_DEVINFO_DATA>(new SP_DEVINFO_DATA(devInfoData));
}

Device::~Device() {
	SetupDiDeleteDeviceInfo(_devInfoSetPtr->devInfoSet(), _devInfoData.get());
}

HDEVINFO Device::devInfoSet() const
{
	if (_devInfoSetPtr)
		return _devInfoSetPtr->devInfoSet();
}

std::list<std::shared_ptr<Driver>> Device::getDriverList() const {
	bool success;
	std::list<std::shared_ptr<Driver>> result;

	success = SetupDiBuildDriverInfoList(_devInfoSetPtr->devInfoSet(), _devInfoData.get(), SPDIT_COMPATDRIVER);// necessary before calling SetupDiEnumDriverInfo

	if (!success)
		return result;// return empty because of the error

	// TODO

	SP_DRVINFO_DATA drvInfo;
	drvInfo.cbSize = sizeof(SP_DRVINFO_DATA);

	for (int i = 0; i < 10; i++) {
		if (!SetupDiEnumDriverInfo(_devInfoSetPtr->devInfoSet(), _devInfoData.get(), SPDIT_COMPATDRIVER, i, &drvInfo)) {// no more drivers for the current device
			break; // no drivers left
		}
		else {
			std::shared_ptr<Driver> drv(new Driver(
				std::wstring(drvInfo.Description),
				std::wstring(drvInfo.ProviderName),
				std::wstring(drvInfo.MfgName),
				std::to_wstring(drvInfo.DriverVersion)
			));
			result.push_back(drv);
		}
	}

	SetupDiDestroyDriverInfoList(_devInfoSetPtr->devInfoSet(), _devInfoData.get(), SPDIT_COMPATDRIVER);
	return result;
}