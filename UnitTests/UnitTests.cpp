#include "pch.h"
#include "CppUnitTest.h"
#include "../common/DeviceShared.h"
#include "../common/DeviceShared.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{
	TEST_CLASS(UnitTests)
	{
	public:
		
		TEST_METHOD(Test_DeviceShared_stringify)
		{
			// Create the device A
			GUID guid;
			guid.Data1 = 1;
			guid.Data2 = 2;
			guid.Data3 = 3;
			for(int i = 0; i < 8; i++)
				guid.Data4[i] = 'A' + i;
			DeviceShared devA(guid, 9999, "Aa", "Bb", "Cc");
			// stringify and create a stream
			std::stringstream str(devA.stringify());
			// restore as the device B
			DeviceShared devB(str);
			// compare 
			Assert::AreEqual<int>(
				devA.guid.Data2,//const T & expected,
				devB.guid.Data2//const T & actual,
				);
			Assert::AreEqual<std::string>(
				devA.name,//const T & expected,
				devB.name//const T & actual,
			);
			Assert::AreEqual<std::string>(
				devA.description,//const T & expected,
				devB.description//const T & actual,
				);
			Assert::AreEqual<std::string>(
				devA.manufacturer,//const T & expected,
				devB.manufacturer//const T & actual,
				);
		}
	};
}
