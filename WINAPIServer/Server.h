#pragma once

#include <string>
#include <vector>
#include <winsock2.h>
#include <ws2tcpip.h>

void runServer(const char* byteArray, int byteArrayL, std::string port);


class Server {
public:
	Server(std::string port);
	~Server();
	void start();
	void disconnect(int client);
	bool isConnected(int client);
	void listenIt();
	void serveAllIt();
	void serveIt(int client);
	void removeAllInvalidClients();
	void cleanUp();
	int clientsCount();
	const int MAX_CLIENTS = 10;
private:
	bool _send(std::string data, int client);
	addrinfo _addrinfo;
	SOCKET _listenSocket;
	std::vector<SOCKET> _clientSocket;
	WSADATA _wsaData;
	std::string _port;
	static const int _buflen = 1028 * 100; // 100 KB buffer
	char _buffer[_buflen];
};