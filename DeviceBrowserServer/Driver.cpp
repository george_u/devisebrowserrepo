#include "Driver.h"


Driver::Driver(
	std::wstring description,
	std::wstring providerName,
	std::wstring mfgName,
	std::wstring version
) : 
	description(description),
	providerName(providerName),
	mfgName(mfgName),
	version(version)
{}