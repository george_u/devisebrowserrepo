/*
	!This file is shared between the client and server
*/
#include "../common/DriverShared.h"

#include <string>
#include <sstream>

DriverShared::DriverShared(
	std::wstring description,
	std::wstring providerName,
	std::wstring mfgName,
	std::wstring version
) :
	description(description), providerName(providerName), mfgName(mfgName), version(version){}

DriverShared::DriverShared(std::stringstream &data, bool& successful)  {
	successful = _parse(data);
}

DriverShared::DriverShared(std::stringstream& data) {
	_parse(data);
}

bool DriverShared::_parse(std::stringstream& data) {
	bool success = true;
	//read the pack size in bytes
	int packSize = 0;
	success &= (data >> packSize) ? true : false;
	// remove an empty line (\n after the number)
	if (success)
		success &= (data.ignore(256, '\n')) ? true : false;

	char* pack = new char[packSize];
	data.read(pack, packSize);
	std::wstringstream packStream(std::wstring((const wchar_t*)pack, packSize / sizeof(wchar_t))); 

	// read the fields
	if (success)
		success &= (std::getline(packStream, description)) ? true : false;
	if (success)
		success &= (std::getline(packStream, providerName)) ? true : false;
	if (success)
		success &= (std::getline(packStream, mfgName)) ? true : false;
	if (success)
		success &= (std::getline(packStream, version)) ? true : false;
	// remove the last empty line
	if (success)
		success &= (data.ignore(256, '\n')) ? true : false;
	if (!success)
		description = L"!!!!error reading";
	return success;
}

std::string DriverShared::stringify() {
	std::stringstream result; // using string stream to construct the responce efficiently

	// pack the strings
	std::wstringstream pack; // using string stream to construct the responce efficiently

	pack << description.c_str() << L'\n';
	pack << providerName.c_str() << L'\n';
	pack << mfgName.c_str() << L'\n';
	pack << version.c_str() << L'\n';

	std::wstring packStr = pack.str();
	int packSize = packStr.size() * sizeof(wchar_t); // size in bytes

	result << '\n' << packSize << '\n';
	result.write((const char*)packStr.c_str(), packSize);

	return result.str();
}