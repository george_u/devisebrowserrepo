#pragma once

#include <list>
#include <memory>

#include "Server.h"
#include "Device.h"

namespace Globals
{
	extern std::list<std::shared_ptr<Device>> deviceList;
	extern DeviceInfoSet DeviceInfo;
	void updateDeviceList();
}
