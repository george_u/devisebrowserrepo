#pragma once

#include <Windows.h> // SetupAPI depends on it
#include <SetupAPI.h>
#include <cfgmgr32.h>
#pragma comment(lib, "Setupapi.lib")
#pragma comment(lib, "Cfgmgr32.lib")

class DeviceInfoSet
{
public:
	DeviceInfoSet();
	~DeviceInfoSet();
	HDEVINFO devInfoSet() const;
private:
	HDEVINFO _devInfoSet;
};

