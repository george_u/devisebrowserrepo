/*
	!This file is shared between the client and server
*/
#pragma once

#include <guiddef.h>
#include <string>

// Represents parameters of a devise and its driver 
class DeviseDriver {
public:
	GUID guid;
	std::wstring description, providerName, mfgName, version;
	DeviseDriver(
		GUID guid,
		std::wstring description,
		std::wstring providerName,
		std::wstring mfgName,
		std::wstring version
	);
};