#pragma once

#include <string>

class FileTransmission
{
public:
	FileTransmission(std::string fname, int fSize);
	std::string getFname() const;
	bool addBytes(const char* data, int dataLength);
	bool isFinished() const;
	bool isValid() const;
	//static bool initTempDir();
	static std::wstring StrToWStr(std::string);
	static std::wstring currentDirectory();
	static void cleanAll();
	void finish();
	
private:
	std::string _fname;
	bool _isFinished, _isValid;
	int _size, _bytesRecieved;
	void* _handle;
};

