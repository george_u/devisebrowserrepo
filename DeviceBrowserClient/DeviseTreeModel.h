#pragma once

#include <QAbstractItemModel>
#include <QIcon>
#include <QPointer>
#include <memory>
#include "../common/DeviceShared.h"

class DeviceTreeItem : public QObject
{
    Q_OBJECT
public:
    DeviceTreeItem(const QVariant& data, std::shared_ptr<DeviceShared> devise, QPointer< DeviceTreeItem> parent = nullptr);
    DeviceTreeItem(const QVariant& data, QPointer< DeviceTreeItem> parent = nullptr);
    ~DeviceTreeItem();

    DeviceTreeItem* child(int number);
    int childCount() const;
    QVariant data() const;
    QVariant DeviceTreeItem::icon() const;
    DeviceTreeItem* parent();
    std::shared_ptr<DeviceShared> device();
    bool DeviceTreeItem::setDevice(std::shared_ptr<DeviceShared> devise);
    bool DeviceTreeItem::setIcon(QIcon icon);
    bool removeChildren(int position, int count);
    int childNumber() const;
    bool setData(int column, const QVariant& value);

private:
    QVector<DeviceTreeItem*> childItems;
    QVariant itemData;
    QIcon itemIcon;
    std::shared_ptr<DeviceShared> devisePtr;
    QPointer< DeviceTreeItem> parentItem;
};


class DeviceTreeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    DeviceTreeModel::DeviceTreeModel(QObject* parent = nullptr);
    ~DeviceTreeModel();
    QVariant data(const QModelIndex& index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
        int role = Qt::DisplayRole) const override;

    QModelIndex index(int row, int column,
    const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex& index) const override;
    std::shared_ptr<DeviceShared> device(const QModelIndex& index) const;

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    bool setRootItem(QPointer<DeviceTreeItem> root);
    Qt::ItemFlags flags(const QModelIndex& index) const override;
    bool DeviceTreeModel::clear();
private:
    DeviceTreeItem* getItem(const QModelIndex& index) const;

    QPointer< DeviceTreeItem> rootItem; // the tree has monopoly on this object
};