#include "RequestToServer.h"
#include <QTcpSocket>

bool RequestToServer(QByteArray &result, QString ip, QString port) {

	quint16 serverPort = port.toInt();

	QTcpSocket socket;
	socket.connectToHost(ip, serverPort);

	const int Timeout = 2 * 1000; // 2 sec timeout

	if (!socket.waitForConnected(Timeout)) {	// blocking the thread by waiting for connection signal
		return false;
	}

	if (!socket.waitForReadyRead(Timeout)) {	// blocking the thread
		return false;
	}

	// data is avaiable, so read
	result = socket.readAll();

	return true;
}