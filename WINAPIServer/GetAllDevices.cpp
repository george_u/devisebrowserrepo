#pragma once

#include "GetAllDevices.h";

#include <initguid.h>   // include before devpropdef.h
#include <Windows.h>
#include <SetupAPI.h>
#include <Devpkey.h>
#include <cfgmgr32.h>
#include <Cfg.h>
#include <memory>

#pragma comment(lib, "Setupapi.lib")

std::list<std::shared_ptr<Device>> getDeviceList(std::shared_ptr<DeviceInfoSet> devInfoSet) {
    std::list<std::shared_ptr<Device>> result;
    int errCode;
    bool success = false;

    SP_DEVINFO_DATA devInfoData;
    devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

    // Cycle through all devices.
    for (int i = 0; ; i++)
    {
        // Get the device info for this device
        success = SetupDiEnumDeviceInfo(devInfoSet->devInfoSet(), i, &devInfoData);
        if (!success)
            break;// finish the cycle because no more devices to list are left

        GUID classGuid = devInfoData.ClassGuid;

        DEVPROPTYPE strPropertyType = DEVPROP_TYPE_STRING; // the API wants it as a pointer

        const DWORD bufsize = 256;

        wchar_t nameStr[bufsize * sizeof(wchar_t)];
        DWORD nameStrSize;

        success = SetupDiGetDeviceProperty(
            devInfoSet->devInfoSet(),
            &devInfoData,
            &DEVPKEY_NAME, // hope to get the name
            &strPropertyType,
            (PBYTE)&nameStr,
            bufsize,
            &nameStrSize, // RequiredSize, not interesting
            0 //This parameter must be set to zero
        );

        if (!success)
            continue;

        wchar_t descStr[bufsize * sizeof(wchar_t)];
        DWORD descStrSize;
        
        success = SetupDiGetDeviceProperty(
            devInfoSet->devInfoSet(),
            &devInfoData,
            &DEVPKEY_Device_DeviceDesc, // hope to get the description
            &strPropertyType,
            (PBYTE)&descStr,
            bufsize,
            &descStrSize, // RequiredSize, not interesting
            0 //This parameter must be set to zero
        );
        if (!success)
            continue;// continue with the next device if something's wron with this one

        wchar_t mfgStr[bufsize * sizeof(wchar_t)];
        DWORD mfgStrSize;

        success = SetupDiGetDeviceProperty(
            devInfoSet->devInfoSet(),
            &devInfoData,
            &DEVPKEY_Device_Manufacturer,
            &strPropertyType,
            (PBYTE)&mfgStr,
            bufsize,
            &mfgStrSize, // RequiredSize, not interesting
            0 //This parameter must be set to zero
        );
        if (!success)
            continue;// continue with the next device if something's wron with this one
        
        auto device = std::make_shared<Device>(
            devInfoSet,
            devInfoData,
            devInfoData.ClassGuid,
            std::wstring(nameStr, nameStrSize / sizeof(wchar_t)),
            std::wstring(descStr, descStrSize / sizeof(wchar_t)),
            std::wstring(mfgStr, mfgStrSize / sizeof(wchar_t))
        );
        // Determine whether the device is active (using Configuration Manager API)
        ULONG devStatus(0), devProblemCode(0);
        CM_Get_DevNode_Status(&devStatus, &devProblemCode, devInfoData.DevInst, 0);
        // #define DN_NO_SHOW_IN_DM    0x40000000  // S: No show in device manager
        //#define CM_PROB_PHANTOM                    (0x0000002D)   // The devinst currently exists only in the registry (not connected)
        if (
            (devStatus & DN_HAS_PROBLEM) &&
            (devProblemCode == CM_PROB_DISABLED)
            ) {
            device->isEnabled = false;
            device->isShown = true; // show disabled ones, so you can turn them on
        }
        else {
            device->isEnabled = true;
            if ((
                (devStatus & DN_NO_SHOW_IN_DM) |
                (devStatus & DN_HAS_PROBLEM)
                ) == 0 && (devStatus & DN_STARTED) != 0
                ) {
                device->isShown = true;// counts as active only if there's no problems
            }
        }
        // debug hardware ids
        //device->getHardwareId();
        
        result.push_back(device);
    }

    return result;
}