#include "DeviseBrowserClient.h"
#include <string>
#include <sstream>
#include <algorithm>

#include <QTreeWidget>
#include <QInputDialog>
#include <QMessageBox>

#include "DialogConnectToServer.h"

DeviseBrowserClient::DeviseBrowserClient(QWidget *parent) // Qt form
    : QMainWindow(parent), _showHidden(false)
{
    ui.setupUi(this);

    ui.treeView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui.treeView, &QAbstractItemView::customContextMenuRequested,
        this, &DeviseBrowserClient::onCustomContextMenu);

    connect(
        ui.actionConnect, &QAction::triggered,
        this, &DeviseBrowserClient::connectServer); // connect menu handler
    connect(
        ui.actionUpdate, &QAction::triggered,
        this, &DeviseBrowserClient::updateServer); // connect menu handler
    connect(
        ui.actionUpdate_with_hidden, &QAction::triggered,
        this, &DeviseBrowserClient::updateServerWithHidden); // connect menu handler

    connect(
        ui.actionShow_hidden, &QAction::triggered,
        this, &DeviseBrowserClient::showHiddenClicked); // connect menu handler

    connect(
        ui.actionSend_files, &QAction::triggered,
        this, &DeviseBrowserClient::sendFiles); // connect menu handler

    // start the timer
    _timer = new QTimer(this);
    connect(_timer, &QTimer::timeout, this, &DeviseBrowserClient::onTimerTicks);
    _timer->start(1000); // will shot every second
}

// This function makes a request to the server and takes text data from it, but also organizes it into a list of categories with devices inside and creates the tree to display
void DeviseBrowserClient::connectServer() {
    // ask for user input
    DialogConnectToServer dialog(this);
    int dialogResult = dialog.exec();
    QString ip;
    QString port;
    if (dialogResult == QDialog::Accepted) { // only if the user pressed OK
        ip = dialog.getIp();
        port = dialog.getPort();

        _client = std::make_shared<Client>(ip, port);
        if (_client->isConnected()) {
            auto deviceList = _client->update(_showHidden);
            // form the tree
            _formTree(deviceList);
        }
        else {
            _client.reset();
            QMessageBox msgBox;
            msgBox.setText("Connection failed!");
            msgBox.exec();
        }
    }
}

void DeviseBrowserClient::sendFiles() {
    QFileDialog dialog(this);
    dialog.setDirectory(QDir::homePath());
    dialog.setFileMode(QFileDialog::ExistingFiles); // ExistingFileSsss - we're asking for one or multiple files
    QStringList fileNames;

    if (dialog.exec()) {
        fileNames = dialog.selectedFiles();
        for (auto&& fname : fileNames) {
            if (_client == nullptr || !_client->isConnected()) {
                QMessageBox msgBox;
                msgBox.setText("No connection");
                msgBox.exec();
                break;
            }
            bool fileSent = _client->sendFile(fname);
            if (!fileSent) {
                QMessageBox msgBox;
                msgBox.setText("Error sending file");
                msgBox.exec();
            }
        }
        QMessageBox msgBox;
        msgBox.setText("Done");
        msgBox.exec();
    }   
}

void DeviseBrowserClient::_formTree(std::list<std::shared_ptr<DeviceShared>>& list) {
    ui.treeView->setModel(nullptr); // remove the model from usage while recreated
    _treeModel.clear();
    QPointer<DeviceTreeItem> rootItem(new DeviceTreeItem("Computer"));// root item named "Computer", treeModel has a monopoly on this object
    // create & populate the categories
    _cats.clear();
    for (auto&& item : list) {
        DeviceCategory devCat(item->guid); // create an empty category
        auto found = std::find_if(
            _cats.begin(),
            _cats.end(),
            [&devCat](const DeviceCategory cat) {
                return devCat.description == cat.description;   // compare class names
            }
        );
        if (found != _cats.end()) {
            found->items.push_back(item);   // if a category with this class exists, add the item into it
        }
        else { // otherwise add this new category
            devCat.items.push_back(item);
            _cats.push_back(devCat);
        }
    }
    // form the tree
    for (auto&& cat : _cats) {
        
        QPointer<DeviceTreeItem> catItem (new DeviceTreeItem(QString::fromStdWString(cat.description), rootItem));// level 1 item - category
        catItem->setIcon(cat.icon);

        for (auto&& item : cat.items) { // note: it is generally preferable to use auto&& in the range based for loops to avoid copies or subtle bugs when the container returns proxy object

            QString isHidden = item->isHidden ? " [hidden]" : "";
            QString isDisabled = !item->isEnabled ? " [disabled]" : "";
            QPointer<DeviceTreeItem> devItem(new DeviceTreeItem(QString::fromStdWString(item->name) + isHidden + isDisabled, item, catItem));// level 2 item - device
            devItem->setIcon(cat.icon);
        }
    }
    _treeModel.setRootItem(rootItem);
    ui.treeView->setModel(&_treeModel); // treeModel will exist as long as the form does because the model is a part of the class
//}

}

void DeviseBrowserClient::onTimerTicks() {
    if (_client != nullptr) {
        // check the connection &
        // ask for hardware updates
        if (_client->isConnected()) {
            bool hwIsUpdated = _client->askAboutUpdates();
            if (hwIsUpdated) {
            //if(true){// to see memory leaks on the server
                updateServer();
            }
        }
        else { 
            QMessageBox msgBox;
            msgBox.setText("Connection's lost");
            msgBox.exec();

            _client.reset(); // delete because it's useless now
        }
    }
    
}

void DeviseBrowserClient::updateServer() {
    if (_client != nullptr && _client->isConnected()) {
        auto deviceList = _client->update(_showHidden);
        // form the tree
        _formTree(deviceList);
    }
}

void DeviseBrowserClient::updateServerWithHidden() {
    if (_client != nullptr && _client->isConnected()) {
        auto deviceList = _client->update(true);
        // form the tree
        _formTree(deviceList);
    }
}

void DeviseBrowserClient::showHiddenClicked() {
    _showHidden = !_showHidden;
    ui.actionShow_hidden->setChecked(_showHidden);
    updateServer();
}

// this handler creates a context menu with "properties"
void DeviseBrowserClient::onCustomContextMenu(const QPoint& point)
{
    QModelIndex index = ui.treeView->indexAt(point);
    if (index.isValid()) {
        std::shared_ptr<DeviceShared> dd = _treeModel.device(index);

        if (dd) {
            QMenu* contextMenu = new QMenu(ui.treeView);
            contextMenu->setAttribute(Qt::WA_DeleteOnClose); // so the new QMenu will delete itself after any of the options is clicked
            QAction* propAction = new QAction("Properties", contextMenu);
            contextMenu->addAction(propAction);
            connect(propAction, &QAction::triggered, this, [&index, this]() {
                this->itemClicked(index);
                });

            QAction* installAction = new QAction("Install", contextMenu);
            contextMenu->addAction(installAction);
            connect(installAction, &QAction::triggered, this, [&index, this]() {
                this->itemClickedInstall(index);
                });
            
            if (dd->isEnabled || dd->isHidden) { // if hidden its unsure if the device is enabled
                QAction* disAction = new QAction("Disable", contextMenu);
                contextMenu->addAction(disAction);
                connect(disAction, &QAction::triggered, this, [&index, this]() {
                    this->itemClickedDisable(index);
                    });
            }
            if(!dd->isEnabled || dd->isHidden){
                QAction* enAction = new QAction("Enable", contextMenu);
                contextMenu->addAction(enAction);
                connect(enAction, &QAction::triggered, this, [&index, this]() {
                    this->itemClickedEnable(index);
                    });
            }
            
            QAction* uninstallAction = new QAction("Uninstall", contextMenu);
            contextMenu->addAction(uninstallAction);
            connect(uninstallAction, &QAction::triggered, this, [&index, this]() {
                this->itemClickedUninstall(index);
                });

            contextMenu->exec(ui.treeView->viewport()->mapToGlobal(point));
        }
    }
}

// this function handles a click on the tree and shall find the element clicked to show its parameters
void DeviseBrowserClient::itemClicked(QModelIndex index) {
    std::shared_ptr<DeviceShared> dd = _treeModel.device(index);
    if (dd) {
        QString text;    // forming text data about the device

        auto drv = _client->driverInfo(dd);
        if(drv != nullptr){
            text = QString::fromStdWString(
                drv->description + L'\n' +
                L"Provider: " + drv->providerName + L'\n' +
                L"Manufacturer: " + drv->mfgName+ L'\n' +
                L"Version: " + drv->version
            );
        }

        QMessageBox msgBox;
        msgBox.setText(text);
        msgBox.exec();
    }
}

void DeviseBrowserClient::itemClickedEnable(QModelIndex index) {
    std::shared_ptr<DeviceShared> dd = _treeModel.device(index);
    if (dd) {
        if (_client->enableDevice(dd))
        {
            QMessageBox msgBox;
            msgBox.setText("Enabled");
            msgBox.exec();
        }
        else {
            QMessageBox msgBox;
            msgBox.setText("Operation failed");
            msgBox.exec();
        }
    }
}


void DeviseBrowserClient::itemClickedDisable(QModelIndex index) {
    std::shared_ptr<DeviceShared> dd = _treeModel.device(index);
    if (dd) {
        if (_client->disableDevice(dd))
        {
            QMessageBox msgBox;
            msgBox.setText("Disabled");
            msgBox.exec();
        }
        else {
            QMessageBox msgBox;
            msgBox.setText("Operation failed");
            msgBox.exec();
        }
    }
}

void DeviseBrowserClient::itemClickedUninstall(QModelIndex index) {
    std::shared_ptr<DeviceShared> dd = _treeModel.device(index);
    if (dd) {
        if (_client->uninstallDevice(dd))
        {
            QMessageBox msgBox;
            msgBox.setText("Uninstalled succesfully");
            msgBox.exec();
        }
        else {
            QMessageBox msgBox;
            msgBox.setText("Operation failed");
            msgBox.exec();
        }
    }
}



void DeviseBrowserClient::itemClickedInstall(QModelIndex index) {
    std::shared_ptr<DeviceShared> dd = _treeModel.device(index);
    if (dd) {
        QFileDialog dialog(this);
        dialog.setDirectory(QDir::homePath());
        dialog.setFileMode(QFileDialog::ExistingFiles); // ExistingFileSsss - we're asking for one or multiple files
        QStringList fileNames;

        if (dialog.exec()) {
            fileNames = dialog.selectedFiles();

            for (auto&& fname : fileNames) {
                QFileInfo info(fname);
                // send
                if (_client == nullptr || !_client->isConnected()) {
                    QMessageBox msgBox;
                    msgBox.setText("No connection");
                    msgBox.exec();
                    break;
                }
                bool fileSent = _client->sendFile(fname);
                if (!fileSent) {
                    QMessageBox msgBox;
                    msgBox.setText("Error sending file\n" + fname);
                    msgBox.exec();
                }
            }
            for (auto&& fname : fileNames) {
                // check if it's the .inf
                QFileInfo info(fname);
                if (info.suffix() == "inf") {
                    if (_client == nullptr || !_client->isConnected()) {
                        QMessageBox msgBox;
                        msgBox.setText("No connection");
                        msgBox.exec();
                        break;
                    }
                    bool drvInstalled = _client->installInf(info.fileName(), dd);
                    if (!drvInstalled) {
                        QMessageBox msgBox;
                        msgBox.setText("Error installing\n" + fname);
                        msgBox.exec();
                    }
                }
            }
            QMessageBox msgBox;
            msgBox.setText("Done");
            msgBox.exec();
        }
    }
}