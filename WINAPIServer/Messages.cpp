#include "Messages.h"

#include <sstream>

#include "../common/DeviceShared.h"
#include "../common/DriverShared.h"
#include "Globals.h"

#include <SetupAPI.h>
#include <Devpkey.h>
#include <cfgmgr32.h>
#include <newdev.h>

#pragma comment(lib, "Setupapi.lib")
#pragma comment(lib, "Newdev.lib")

namespace Messages {
	bool _installDrv(std::wstring fname, std::wstring devClass);
	void _writeDeviceIntoString(std::shared_ptr<Device>& item, std::stringstream& stream);
	void _writeDriverIntoString(std::shared_ptr<Driver>& item, std::stringstream& stream);
	
	
	std::string takeDeviceList() {
		//update the list
		Globals::updateDeviceList();
		std::stringstream stream;
		stream << "TAKE_DEVICE_LIST\n";
		for (auto& item : Globals::deviceList) {
			if (item->isShown) {
				_writeDeviceIntoString(item, stream);
			}
		}
		return stream.str();
	}

	std::string takeDeviceListWithHidden() {
		//update the list
		Globals::updateDeviceList();
		std::stringstream stream;
		stream << "TAKE_DEVICE_LIST\n";
		for (auto& item : Globals::deviceList) {
			_writeDeviceIntoString(item, stream);
		}
		return stream.str();
	}


	std::string takeDriverProperties(std::stringstream& stream) {
		// Analyze GUID
		DeviceShared dev(stream); // parse the device we've got
		if (!stream)
			return "Error getting data"; // wrong data format from the client
		// Find the device
		auto found = std::find_if(
			Globals::deviceList.begin(),
			Globals::deviceList.end(),
			[&dev](std::shared_ptr<Device> devPtr) {
				return /*dev.guid == devPtr->guid && */dev.instanceCode == devPtr->instanceCode;
			});
		std::shared_ptr<Device> devPtr = *found;
		if (found == Globals::deviceList.end())
			return "Error getting data"; // no such device found
		// Extract driver info
		if(devPtr == nullptr)
			return "Error getting data"; // bad pointer
		std::list<std::shared_ptr<Driver>> drvList = devPtr->getDriverList();
		if(drvList.size() == 0)
			return "Error getting data"; // no drivers found
		// Forming responce
		std::stringstream responce;
		responce << "TAKE_DRIVER_PROPERTIES\n";
		// Driver -> string
		_writeDriverIntoString(*(drvList.begin()), responce);
		return responce.str();
	}

	std::string enableDevice(std::stringstream& stream) {
		const std::string success = "SUCCESS";
		const std::string failure = "FAILURE";
		// Analyze GUID
		DeviceShared dev(stream); // parse the device we've got
		if (!stream)
			return failure; // wrong data format from the client
		// Find the device
		auto found = std::find_if(
			Globals::deviceList.begin(),
			Globals::deviceList.end(),
			[&dev](std::shared_ptr<Device> devPtr) {
				return /*dev.guid == devPtr->guid && */dev.instanceCode == devPtr->instanceCode;
			});
		std::shared_ptr<Device> devPtr = *found;
		if (found == Globals::deviceList.end())
			return failure; // no such device found
		
		if (devPtr == nullptr)
			return failure; // bad pointer

		bool toggleResult = devPtr->enable();

		if(toggleResult)
			return success;
		return failure;
	}

	std::string disableDevice(std::stringstream& stream) {
		const std::string success = "SUCCESS";
		const std::string failure = "FAILURE";
		// Analyze GUID
		DeviceShared dev(stream); // parse the device we've got
		if (!stream)
			return failure; // wrong data format from the client
		// Find the device
		auto found = std::find_if(
			Globals::deviceList.begin(),
			Globals::deviceList.end(),
			[&dev](std::shared_ptr<Device> devPtr) {
				return /*dev.guid == devPtr->guid && */dev.instanceCode == devPtr->instanceCode;
			});
		std::shared_ptr<Device> devPtr = *found;
		if (found == Globals::deviceList.end())
			return failure; // no such device found

		if (devPtr == nullptr)
			return failure; // bad pointer

		bool toggleResult = devPtr->disable();

		if (toggleResult)
			return success;
		return failure;
	}

	std::string uninstallDevice(std::stringstream& stream) {
		const std::string success = "SUCCESS";
		const std::string failure = "FAILURE";
		// Analyze GUID
		DeviceShared dev(stream); // parse the device we've got
		if (!stream)
			return failure; // wrong data format from the client
		// Find the device
		auto found = std::find_if(
			Globals::deviceList.begin(),
			Globals::deviceList.end(),
			[&dev](std::shared_ptr<Device> devPtr) {
				return /*dev.guid == devPtr->guid && */dev.instanceCode == devPtr->instanceCode;
			});
		std::shared_ptr<Device> devPtr = *found;
		if (found == Globals::deviceList.end())
			return failure; // no such device found

		if (devPtr == nullptr)
			return failure; // bad pointer

		bool toggleResult = devPtr->uninstall();

		if (toggleResult)
			return success;
		return failure;
	}

	std::string isUpdated() {
		if (Globals::isUpdated) {
			Globals::isUpdated = false;
			return "IS_UPDATED\nYES";
		}	
		return "IS_UPDATED\nNO";
	}

	void _writeDeviceIntoString(std::shared_ptr<Device>& item, std::stringstream& stream) {
		// wstring -> string conversions
		/*int i;
		std::string nameStr(item->name.size(), ' '); i = 0;
		for (char c : item->name) nameStr[i++] = c;
		std::string descStr(item->description.size(), ' '); i = 0;
		for (char c : item->description) descStr[i++] = c;
		std::string mfgStr(item->manufacturer.size(), ' '); i = 0;
		for (char c : item->manufacturer) mfgStr[i++] = c;*/
		// Creating shared device object
		if (item->name == L"")
			item->name = item->description;
		DeviceShared devShared(item->guid, item->instanceCode, item->name, item->description, item->manufacturer, /*hidden = */!(item->isShown), item->isEnabled);
		// Adding to the result
		stream << devShared.stringify();
	}

	void _writeDriverIntoString(std::shared_ptr<Driver>& item, std::stringstream& stream) {
		// wstring -> string conversions
		int i;
		/*std::string descStr(item->description.size(), ' '); i = 0;
		for (char c : item->description) descStr[i++] = c;
		std::string provStr(item->providerName.size(), ' '); i = 0;
		for (char c : item->providerName) provStr[i++] = c;
		std::string mfgStr(item->mfgName.size(), ' '); i = 0;
		for (char c : item->mfgName) mfgStr[i++] = c;
		std::string versionStr(item->version.size(), ' '); i = 0;
		for (char c : item->version) versionStr[i++] = c;*/
		// Creating shared driver object
		DriverShared drvShared(item->description, item->providerName, item->mfgName, item->version);
		// Adding to the result
		stream << drvShared.stringify();
	}


	std::string initFile(std::stringstream& stream) {
		const std::string success = "SUCCESS";
		const std::string failure = "FAILURE";
		// get the file name
		std::string nameStr;
		bool ok = (std::getline(stream, nameStr)) ? true : false;
		if (!ok) return failure;
		// get the file size
		int fileSize = 0;
		ok = (stream >> fileSize) ? true : false;
		if (!ok) return failure;
		// create a transmission object
		auto ft = std::make_shared<FileTransmission>(nameStr, fileSize);
		if(!ft->isValid()) return failure;
		// add it to the list
		Globals::fileTransmissions.push_back(ft);
		return success;
	}

	std::string writeFile(std::stringstream& stream) {
		const std::string success = "SUCCESS";
		const std::string failure = "FAILURE";
		// get the file name
		std::string nameStr;
		bool ok = (std::getline(stream, nameStr)) ? true : false;
		if (!ok) return failure;
		// get the number of bytes
		int number = 0;
		if(!(stream >> number)) return failure;
		// skip a line
		ok = (stream.ignore(256, '\n')) ? true : false;
		// check if it's open and valid
		ok = false;
		for (auto&& ft : Globals::fileTransmissions) {
			if (ft != nullptr && ft->getFname() == nameStr && ft->isValid() && !ft->isFinished()) {
				char* buffer = new char[number];
				stream.read(buffer, number);
				ok = ft->addBytes(buffer, number);
				delete []buffer;
				break;
			}
		}
		if(!ok) return failure;
		return success;
	}
	std::string finishFile(std::stringstream& stream) {
		const std::string success = "SUCCESS";
		const std::string failure = "FAILURE";
		// get the file name
		std::string nameStr;
		bool ok = (std::getline(stream, nameStr)) ? true : false;
		if (!ok) return failure;
		
		// check if it's open and valid
		ok = false;
		for (auto&& ft : Globals::fileTransmissions) {
			if (ft != nullptr && ft->getFname() == nameStr && ft->isValid() && !ft->isFinished()) {
				ft->finish();
				ok = true;
				break;
			}
		}
		if (!ok) return failure;
		return success;
	}

	std::string installInf(std::stringstream& stream) {
		const std::string success = "SUCCESS";
		const std::string failure = "FAILURE";
		// get the file name
		std::string nameStr;
		bool ok = (std::getline(stream, nameStr)) ? true : false;
		if (!ok) return failure;
		// Analyze GUID
		DeviceShared dev(stream); // parse the device we've got
		if (!stream)
			return failure; // wrong data format from the client
		// Find the device
		auto found = std::find_if(
			Globals::deviceList.begin(),
			Globals::deviceList.end(),
			[&dev](std::shared_ptr<Device> devPtr) {
				return /*dev.guid == devPtr->guid && */dev.instanceCode == devPtr->instanceCode;
			});
		std::shared_ptr<Device> devPtr = *found;
		if (found == Globals::deviceList.end())
			return failure; // no such device found

		if (devPtr == nullptr)
			return failure; // bad pointer
		//find its hardware ids
		auto ids = devPtr->getHardwareId();
		// check if it's open and valid
		ok = false;
		//for (auto&& ft : Globals::fileTransmissions) {
		//	if (ft != nullptr && ft->getFname() == nameStr && ft->isValid() && !ft->isFinished()) {
				// file found
				std::wstringstream nameWStr;
				for (auto c : nameStr) nameWStr << c;
				std::wstring fullPath = Globals::tempDirectory + L'\\' + nameWStr.str().c_str();
				//try to install the file for any of the ids
				for (auto&& id : ids) {
					ok = _installDrv(fullPath, id);
					if (ok) {
						break;// congrats
					}
				}
				
		//		break;
		//	}
		//}

		if (!ok) return failure;
		return success;
	}

	bool _installDrv(std::wstring fname, std::wstring devClass) {
		// Unicode-less version
		bool success = false;
		success = DiInstallDriver(
			NULL,//HWND   hwndParent,
			fname.c_str(),//LPCSTR InfPath,
			DIIRFLAG_FORCE_INF,//DWORD  Flags,
			NULL//PBOOL  NeedReboot
		);
		if (!success) {
			return false;
		}
		return true;
	}

}
