#pragma once

#include <list>
#include <memory>
#include "../common/DeviseDriver.h"
#include "Device.h"
#include "Driver.h"


//std::list<std::shared_ptr<DeviseDriver>> getAllDevices();

std::list<std::shared_ptr<Device>> getDeviceList(const DeviceInfoSet* devInfoSet);