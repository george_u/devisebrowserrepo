/*
	!This file is shared between the client and server
*/
#pragma once

#include <string>
#include <guiddef.h>

class DeviceShared {
public:
	GUID guid;
	long instanceCode;
	std::wstring name, description, manufacturer;
	bool isHidden, isEnabled;
	DeviceShared(
		GUID guid,
		long instanceCode,
		std::wstring name,
		std::wstring description,
		std::wstring manufacturer
	);
	DeviceShared(
		GUID guid,
		long instanceCode,
		std::wstring name,
		std::wstring description,
		std::wstring manufacturer,
		bool hidden,
		bool enabled
	);
	DeviceShared(std::stringstream& data);
	DeviceShared(std::stringstream& data, bool& success);
	std::string stringify();
private:
	bool _parse(std::stringstream& data);
};

