#include "Main.h"

#include <iostream>
#include <sstream> // (wide) string stream


int main() {
	std::wcout << "Please, enter the port number^^\n";
	std::string portNumber;
	std::cin >> portNumber;

	std::wcout << "Wait, collecting data about the drivers...";
	Globals::updateDeviceList();

	std::wcout << "GO!\n";

	auto server = std::make_unique<Server>(portNumber);
	server->run();

	return 0;
}