#include "Messages.h"

#include <sstream>

#include "../common/DeviceShared.h"
#include "../common/DriverShared.h"
#include "Globals.h"

namespace Messages {
	void _writeDeviceIntoString(std::shared_ptr<Device>& item, std::stringstream& stream);
	void _writeDriverIntoString(std::shared_ptr<Driver>& item, std::stringstream& stream);
	
	
	std::string takeDeviceList() {
		//update the list
		Globals::updateDeviceList();
		std::stringstream stream;
		stream << "TAKE_DEVICE_LIST\n";
		for (auto& item : Globals::deviceList) {
			if (item->isShown) {
				_writeDeviceIntoString(item, stream);
			}
		}
		return stream.str();
	}

	std::string takeDeviceListWithHidden() {
		//update the list
		Globals::updateDeviceList();
		std::stringstream stream;
		stream << "TAKE_DEVICE_LIST\n";
		for (auto& item : Globals::deviceList) {
			_writeDeviceIntoString(item, stream);
		}
		return stream.str();
	}


	std::string takeDriverProperties(std::stringstream& stream) {
		// Analyze GUID
		DeviceShared dev(stream); // parse the device we've got
		if (!stream)
			return "Error getting data"; // wrong data format from the client
		// Find the device
		auto found = std::find_if(
			Globals::deviceList.begin(),
			Globals::deviceList.end(),
			[&dev](std::shared_ptr<Device> devPtr) {
				return /*dev.guid == devPtr->guid && */dev.instanceCode == devPtr->instanceCode;
			});
		std::shared_ptr<Device> devPtr = *found;
		if (found == Globals::deviceList.end())
			return "Error getting data"; // no such device found
		// Extract driver info
		if(devPtr == nullptr)
			return "Error getting data"; // bad pointer
		std::list<std::shared_ptr<Driver>> drvList = devPtr->getDriverList();
		if(drvList.size() == 0)
			return "Error getting data"; // no drivers found
		// Forming responce
		std::stringstream responce;
		responce << "TAKE_DRIVER_PROPERTIES\n";
		// Driver -> string
		_writeDriverIntoString(*(drvList.begin()), responce);
		return responce.str();
	}

	void _writeDeviceIntoString(std::shared_ptr<Device>& item, std::stringstream& stream) {
		// wstring -> string conversions
		int i;
		std::string nameStr(item->name.size(), ' '); i = 0;
		for (char c : item->name) nameStr[i++] = c;
		std::string descStr(item->description.size(), ' '); i = 0;
		for (char c : item->description) descStr[i++] = c;
		std::string mfgStr(item->manufacturer.size(), ' '); i = 0;
		for (char c : item->manufacturer) mfgStr[i++] = c;
		// Creating shared device object
		if (nameStr == "")
			nameStr = descStr;
		DeviceShared devShared(item->guid, item->instanceCode, nameStr, descStr, mfgStr);
		// Adding to the result
		stream << devShared.stringify();
	}

	void _writeDriverIntoString(std::shared_ptr<Driver>& item, std::stringstream& stream) {
		// wstring -> string conversions
		int i;
		std::string descStr(item->description.size(), ' '); i = 0;
		for (char c : item->description) descStr[i++] = c;
		std::string provStr(item->providerName.size(), ' '); i = 0;
		for (char c : item->providerName) provStr[i++] = c;
		std::string mfgStr(item->mfgName.size(), ' '); i = 0;
		for (char c : item->mfgName) mfgStr[i++] = c;
		std::string versionStr(item->version.size(), ' '); i = 0;
		for (char c : item->version) versionStr[i++] = c;
		// Creating shared driver object
		DriverShared drvShared(descStr, provStr, mfgStr, versionStr);
		// Adding to the result
		stream << drvShared.stringify();
	}

}

