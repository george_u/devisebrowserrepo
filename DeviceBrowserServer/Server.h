#pragma once

#include <string>
#include <winsock2.h>
#include <ws2tcpip.h>

void runServer(const char* byteArray, int byteArrayL, std::string port);


class Server {
public:
	Server(std::string port);
	~Server();
	void run();
	
private:
	void _listen();
	void _serve();
	bool _send(std::string data);
	addrinfo _addrinfo;
	SOCKET _clientSocket;
	WSADATA _wsaData;
	std::string _port;
	static const int _buflen = 1028 * 100; // 100 KB buffer
	char _buffer[_buflen];
};