#pragma once

#include "GetAllDevices.h";

#include <initguid.h>   // include before devpropdef.h
#include <Windows.h>
#include <SetupAPI.h>
#include <Devpkey.h>
#include <cfgmgr32.h>
#include <Cfg.h>
#include <memory>

#pragma comment(lib, "Setupapi.lib")
//
//DeviseDriver DeviceDriverFromData(PSP_DEVINFO_DATA devInfo, PSP_DRVINFO_DATA drvInfo) {
//    return DeviseDriver(
//        (*devInfo).ClassGuid,
//        std::wstring(drvInfo->Description),
//        std::wstring(drvInfo->ProviderName),
//        std::wstring(drvInfo->MfgName),
//        std::to_wstring(drvInfo->DriverVersion)
//    );
//}
//
//std::list<std::shared_ptr<DeviseDriver>> getAllDevices() {
//    std::list<std::shared_ptr<DeviseDriver>> toReturn;
//    HDEVINFO devInfoSet = SetupDiGetClassDevs(NULL, NULL, NULL,
//        DIGCF_ALLCLASSES);
//
//
//    // Cycle through all devices.
//    for (int i = 0; ; i++)
//    {
//        // Get the device info for this device
//        SP_DEVINFO_DATA devInfo;
//        devInfo.cbSize = sizeof(SP_DEVINFO_DATA);
//        if (!SetupDiEnumDeviceInfo(devInfoSet, i, &devInfo))
//            break;
//
//        SetupDiBuildDriverInfoList(devInfoSet, &devInfo, SPDIT_COMPATDRIVER);// necessary before calling SetupDiEnumDriverInfo
//
//        // Get the first info item for this driver
//        SP_DRVINFO_DATA drvInfo;
//        drvInfo.cbSize = sizeof(SP_DRVINFO_DATA);
//        for (int j = 0; j < 2; j++) {  
//            // for whatever reason Windows return 2 identical DriverInfo for each device
//
//            if (!SetupDiEnumDriverInfo(devInfoSet, &devInfo, SPDIT_COMPATDRIVER, j, &drvInfo)) {// no more drivers for the current device
//                break;
//            }
//            else {
//                std::shared_ptr<DeviseDriver> data(
//                    new DeviseDriver(DeviceDriverFromData(&devInfo, &drvInfo))
//                );
//                toReturn.push_back(data);
//            }
//        }
//    }
//
//    SetupDiDestroyDeviceInfoList(devInfoSet);
//
//    return toReturn;
//}





std::list<std::shared_ptr<Device>> getDeviceList(const DeviceInfoSet* devInfoSet) {
    std::list<std::shared_ptr<Device>> result;
    int errCode;
    bool success = false;

    SP_DEVINFO_DATA devInfoData;
    devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

    // Cycle through all devices.
    for (int i = 0; ; i++)
    {
        // Get the device info for this device
        success = SetupDiEnumDeviceInfo(devInfoSet->devInfoSet(), i, &devInfoData);
        if (!success)
            break;// finish the cycle because no more devices to list are left

        GUID classGuid = devInfoData.ClassGuid;

        DEVPROPTYPE strPropertyType = DEVPROP_TYPE_STRING; // the API wants it as a pointer

        const DWORD bufsize = 256;


        wchar_t nameStr[bufsize * sizeof(wchar_t)];
        auto nameStrSize = new DWORD();

        success = SetupDiGetDeviceProperty(
            devInfoSet->devInfoSet(),
            &devInfoData,
            &DEVPKEY_NAME, // hope to get the name
            &strPropertyType,
            (PBYTE)&nameStr,
            bufsize,
            nameStrSize, // RequiredSize, not interesting
            0 //This parameter must be set to zero
        );

        //SetupDiGetDeviceRegistryPropertyA(
        //    devInfoSet->devInfoSet(), //HDEVINFO         DeviceInfoSet,
        //    &devInfoData, //PSP_DEVINFO_DATA DeviceInfoData,
        //    SPDRP_FRIENDLYNAME, //DWORD            Property,
        //    NULL, //PDWORD           PropertyRegDataType,
        //    (PBYTE)&nameStr, //PBYTE            PropertyBuffer,
        //    bufsize,//DWORD            PropertyBufferSize,
        //    nameStrSize//PDWORD           RequiredSize
        //);

        if (!success)
            continue;// continue with the next device if something's wron with this one
            //nameStr[0] = '\0';// continue with the next device if something's wron with this one

        // use Config Mgr to get a nice string to compare against
        //wchar_t devId[256];
        //DWORD devIdSize;

        //auto devIdResult = CM_Get_Device_ID(devInfoData.DevInst, (PWSTR)&devId, 256, 0);
        //if (devIdResult == CR_SUCCESS) {
        //    CM_Get_Device_ID_Size(
        //        &devIdSize,
        //        devInfoData.DevInst,
        //        0 // not used, must be zero
        //    );
        //}
        //else {
        //    continue;// continue with the next device if something's wron with this one
        //}

        wchar_t descStr[bufsize * sizeof(wchar_t)];
        auto descStrSize = new DWORD();
        
        success = SetupDiGetDeviceProperty(
            devInfoSet->devInfoSet(),
            &devInfoData,
            &DEVPKEY_Device_DeviceDesc, // hope to get the description
            &strPropertyType,
            (PBYTE)&descStr,
            bufsize,
            descStrSize, // RequiredSize, not interesting
            0 //This parameter must be set to zero
        );
        if (!success)
            continue;// continue with the next device if something's wron with this one

        wchar_t mfgStr[bufsize * sizeof(wchar_t)];
        auto mfgStrSize = new DWORD();

        success = SetupDiGetDeviceProperty(
            devInfoSet->devInfoSet(),
            &devInfoData,
            &DEVPKEY_Device_Manufacturer,
            &strPropertyType,
            (PBYTE)&mfgStr,
            bufsize,
            mfgStrSize, // RequiredSize, not interesting
            0 //This parameter must be set to zero
        );
        if (!success)
            continue;// continue with the next device if something's wron with this one
        
        auto device = std::make_shared<Device>(
            devInfoSet,
            devInfoData,
            devInfoData.ClassGuid,
            std::wstring(nameStr, *nameStrSize / sizeof(wchar_t)),
            std::wstring(descStr, *descStrSize / sizeof(wchar_t)),
            std::wstring(mfgStr, *mfgStrSize / sizeof(wchar_t))
        );
        // Determine whether the device is active (using Configuration Manager API)
        ULONG devStatus(0), devProblemCode(0);
        CM_Get_DevNode_Status(&devStatus, &devProblemCode, devInfoData.DevInst, 0);
        // #define DN_NO_SHOW_IN_DM    0x40000000  // S: No show in device manager
        //#define CM_PROB_PHANTOM                    (0x0000002D)   // The devinst currently exists only in the registry (not connected)
        if((
            (devStatus & DN_NO_SHOW_IN_DM) |
            (devStatus & DN_HAS_PROBLEM)
            ) == 0 && (devStatus & DN_STARTED) != 0
            ) {
            device->isShown = true;// counts as active only if there's no problems
        }
        
        result.push_back(device);
        delete nameStrSize, mfgStrSize, descStrSize;
    }

    return result;
}