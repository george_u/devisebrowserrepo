#include <Windows.h>

#include "DeviseCategory.h"
#include <SetupAPI.h>
#include <WinUser.h>	// only to destroy icons
#include <QtWinExtras>	// to get icons
#include <sstream>

#pragma comment(lib, "Setupapi.lib")

// the constructor takes the guid and gets the class name and icon for the corresponding class
DeviceCategory::DeviceCategory(GUID guid)
	: guid (guid)
{
	//this->guid = guid;
	const int size = 1024;
	DWORD realSize;
	wchar_t* className = new wchar_t[size];
	bool result = SetupDiClassNameFromGuid(&guid, className, size, &realSize);
	if (result) {
		this->description = std::wstring(className, realSize);
	}
	else {
		this->description = std::wstring(L"Error reading guid class name");
	}
	HICON largeIcon;
	result = SetupDiLoadClassIcon(&guid, &largeIcon, NULL);
	if (result) {
		//QPixmap pixmap = QPixmap::fromImage(QImage::fromHICON(icon)); // for qt6
		QPixmap pixmap = QtWin::fromHICON(largeIcon);
		this->icon = QIcon(pixmap);
		DestroyIcon(largeIcon);	// free space
	}
	else {
		this->icon = QIcon();
	}
	delete[] className;
}


/*
// the server will return text data, this function recreates the device list from it
std::list<std::shared_ptr<DeviceShared>> StringDataToDeviseList(const std::wstring &data) {
	std::wstring line;
	std::wstringstream dataStream(data);
	std::list<DeviseDriver> toReturn;
	do {
        if ((unsigned)dataStream.tellg() >= data.length())
			break;
		GUID guid;
		std::wstring description;
		std::wstring providerName;
		std::wstring mfgName;
		std::wstring version;
		// guid
		std::getline(dataStream, line);
		std::wstringstream guidStream(line);

		guidStream >> guid.Data1 >> guid.Data2 >> guid.Data3;// >> chars;
		for (int i = 0; i < 8; i++) {
			unsigned int num;
			guidStream >> num;
			guid.Data4[i] = num;
		}

        if ((unsigned)dataStream.tellg() >= data.length())
			break;
		// description
		std::getline(dataStream, line);
		description = line;

        if ((unsigned)dataStream.tellg() >= data.length())
			break;
		// version
		std::getline(dataStream, line);
		version = line;

        if ((unsigned)dataStream.tellg() >= data.length())
			break;
		// providerName
		std::getline(dataStream, line);
		providerName = line;

        if ((unsigned)dataStream.tellg() >= data.length())
			break;
		// mfgName
		std::getline(dataStream, line);
		mfgName = line;

        if ((unsigned)dataStream.tellg() >= data.length())
			break;
		// empty line
		std::getline(dataStream, line);
		toReturn.push_back(
			DeviseDriver(
				guid,
				description,
				providerName,
				mfgName,
				version
			)
		);
	} while (true); // will stop if the end of file is reached

	return toReturn;
}
*/