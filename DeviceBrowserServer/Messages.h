#pragma once

#include <string>

namespace Messages {
	std::string takeDeviceList();
	std::string takeDeviceListWithHidden();
	std::string takeDriverProperties(std::stringstream& stream);
}