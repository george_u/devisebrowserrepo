#include "Globals.h"

#include "GetAllDevices.h"

namespace Globals
{
	std::list<std::shared_ptr<Device>> deviceList;
	std::shared_ptr<DeviceInfoSet> DeviceInfo;
	std::string portNumber = "100";
	std::wstring tempDirectory;
	std::list<std::shared_ptr<FileTransmission>> fileTransmissions;
	bool isUpdated;

	void updateDeviceList() {
		// recreate
		DeviceInfo = std::make_shared<DeviceInfoSet>();
		deviceList.clear();
		
		// make the list
		deviceList = getDeviceList(Globals::DeviceInfo);
	}
};
