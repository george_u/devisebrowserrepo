#pragma once

#include <string>

namespace Messages {
	std::string takeDeviceList();
	//std::string takeFile();
	std::string takeDeviceListWithHidden();

	std::string takeDriverProperties(std::stringstream& stream);
	std::string isUpdated();

	std::string enableDevice(std::stringstream& stream);
	std::string disableDevice(std::stringstream& stream);
	std::string uninstallDevice(std::stringstream& stream);

	std::string initFile(std::stringstream& stream);
	std::string writeFile(std::stringstream& stream);
	std::string finishFile(std::stringstream& stream);

	std::string installInf(std::stringstream& stream);
}