#pragma once

#include <vector>
#include <guiddef.h>
#include <QtWidgets>
#include "../common/DeviceShared.h"

// represents a class of devices with certain form of GUID (windows knows to which class which GUID belongs)
class DeviceCategory {
public:
	GUID guid;	// GUID of a random devise belonging to the devise class
	QIcon icon;	// the class icon (from windows)
	std::vector<std::shared_ptr<DeviceShared>> items; // array of devises
	std::wstring description;
	DeviceCategory(GUID);
}; 

//std::list<std::shared_ptr<DeviceShared>> StringDataToDeviseList(const std::wstring &data);