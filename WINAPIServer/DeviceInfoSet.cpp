#include "DeviceInfoSet.h"

// Calls setupAPI to create a device info set, obtains its HDEVINFO
DeviceInfoSet::DeviceInfoSet()
{
	_devInfoSet = SetupDiGetClassDevs(NULL, NULL, NULL,
		DIGCF_ALLCLASSES);
}

// Calls setupAPI to destroy the device info set
DeviceInfoSet::~DeviceInfoSet()
{
	// Despite not destroying the list may cause a memory leak, destroying it causes
	// an obscure bug, when Windows doesn't wanna give the list again
	
	bool success = SetupDiDestroyDeviceInfoList(_devInfoSet);
}

HDEVINFO DeviceInfoSet::devInfoSet() const
{
	return _devInfoSet;
}