#pragma once
#include "Server.h"

#include <iostream>
#include <sstream>
#include <stdio.h>
#include <conio.h>

// ��� ���������� ������ freeaddrinfo � MinGW
// ���������: http://stackoverflow.com/a/20306451
#define _WIN32_WINNT 0x501

#pragma comment(lib, "Ws2_32.lib")

#include "Messages.h"


void Server::start() {
	int errCode;
	// Create a SOCKET for the server to listen for client connections
	_listenSocket = socket(_addrinfo.ai_family, _addrinfo.ai_socktype, _addrinfo.ai_protocol);

	if (_listenSocket == INVALID_SOCKET) {
		printf("Error at socket(): %ld\n", WSAGetLastError());
		freeaddrinfo(&_addrinfo);
		return;
	}

	// Setup the TCP listening socket
	errCode = bind(_listenSocket, _addrinfo.ai_addr, (int)_addrinfo.ai_addrlen);

	if (errCode == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		closesocket(_listenSocket);
		return;
	}

	// Start listening
	if (listen(_listenSocket, SOMAXCONN) == SOCKET_ERROR) {
		printf("Listen failed with error: %ld\n", WSAGetLastError());
		closesocket(_listenSocket);
		return;
	}

	//SOCKET ClientSocket;	// A SOCKET for accepting clients connections (now global)
	//printf("The server is listening\nPress anything to stop...\n");


}

int Server::clientsCount() {
	return _clientSocket.size();
}

Server::Server(std::string port) :  _port(port), _listenSocket(INVALID_SOCKET), _clientSocket(0)
{
	int errCode;
	// Initialize Winsock
	errCode = WSAStartup(MAKEWORD(2, 2), &_wsaData);
	/* The MAKEWORD(2,2) parameter of WSAStartup makes a request for version 2.2 of Winsock on the system,
	and sets the passed version as the highest version of Windows Sockets support that the caller can use */
	if (errCode != NO_ERROR) {
		printf("WSAStartup failed: %d\n", errCode);
		return;
	}

	struct addrinfo* result = NULL, * ptr = NULL, hints;

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the local address and port to be used by the server
	errCode = getaddrinfo(NULL, _port.c_str(), &hints, &result);
	if (errCode != NO_ERROR) {
		printf("getaddrinfo failed: %d\n", errCode);
		return;
	}

	// save the result
	_addrinfo = *result;
}

bool Server::isConnected(int client) {
	return _clientSocket[client] != INVALID_SOCKET;
}

Server::~Server() {
	cleanUp(); // Cleanup winsock objects
}

void Server::listenIt() {
	int errCode = 0;
	//while (!_kbhit()) {	// _kbhi() from conio.h will return TRUE if the user typed something 

		// Checking for pending connections
		timeval timeout;
		timeout.tv_sec = 0;
		timeout.tv_usec = 10000; // 0.01 sec

		fd_set readySockets;
		FD_ZERO(&readySockets); // empty fd_set
		FD_SET(_listenSocket, &readySockets); // specify which socket we are interested in

		errCode = select(
			NULL, // reserved
			&readySockets,
			NULL,
			NULL,
			&timeout
		);	// select() will mark in fd_set which sockets are ready to recv/accept
		if (errCode == SOCKET_ERROR) {
			printf("failed select, error: %d\n", WSAGetLastError());
			//closesocket(_listenSocket);
			return;
		}
		bool isReady = FD_ISSET(_listenSocket, &readySockets); // will be true if the listening socket can accept without blocking

		if (isReady) {
			// Accept a client socket
			SOCKET newClientSocket = accept(_listenSocket, NULL, NULL);
			if (newClientSocket == INVALID_SOCKET) {
				printf("accept failed: %d\n", WSAGetLastError());
				//closesocket(_listenSocket);
			}
			else if (_clientSocket.size() >= MAX_CLIENTS) {
				printf("too many clients\n");
			}
			else {
				_clientSocket.push_back(newClientSocket);
				printf("a client connected, %d connections\n", clientsCount());
			}
			//closesocket(_listenSocket);
			return; // now we can work with the client
		}
	//}
	//closesocket(ListenSocket);
}

void Server::serveAllIt() {
	for (int i = 0; i < _clientSocket.size(); i++) {
		if(isConnected(i))
			serveIt(i);
	}
}


void Server::removeAllInvalidClients() {
	//std::remove(_clientSocket.begin(), _clientSocket.end(), INVALID_SOCKET);
	std::_Erase_remove_if(
		_clientSocket,
		[](SOCKET s) {
			return s == INVALID_SOCKET;
		}
	);

}

void Server::serveIt(int client) {
	int errCode;
	bool success;

	//while (!_kbhit()) {	// _kbhi() from conio.h will return TRUE if the user typed something 
		// Checking for data ready
		timeval timeout;
		timeout.tv_sec = 0;
		timeout.tv_usec = 10000; // 0.01 sec

		fd_set readySockets;
		FD_ZERO(&readySockets); // empty fd_set
		FD_SET(_clientSocket[client], &readySockets); // specify which socket we are interested in

		errCode = select(
			NULL, // reserved
			&readySockets,
			NULL,
			NULL,
			&timeout
		);	// select() will mark in fd_set which sockets are ready to recv/accept
		if (errCode == SOCKET_ERROR) {
			printf("failed select, error: %d\n", WSAGetLastError());
			disconnect(client);
			//closesocket(_clientSocket[client]);
			//WSACleanup();
			return;
		}
		bool isReady = FD_ISSET(_clientSocket[client], &readySockets); // will be true if the socket have something to read

		if (isReady) {// there's something to read and respond
			//const int buflen = 1028*100; // 100 KB buffer

			//char buffer[buflen];
			// read the actual data
			int recievedBytes = recv(_clientSocket[client], _buffer, _buflen, 0);
			if (recievedBytes > 0) {
				std::stringstream data(std::string(_buffer, recievedBytes));

				// start processing the message
				std::string messageLine;
				if (std::getline(data, messageLine)) {// message line exists
					if (messageLine == "GIVE_DEVICE_LIST") {
						//  finding and sending the device list
						_send(Messages::takeDeviceList(), client);
					}
					else if(messageLine == "GIVE_DEVICE_LIST_WITH_HIDDEN") {
						//  finding and sending the device list
						_send(Messages::takeDeviceListWithHidden(), client);
					}
					else if (messageLine == "GIVE_DRIVER_PROPERTIES") {
						//  finding and sending the driver info for the device asked
						_send(Messages::takeDriverProperties(data), client);
					}
					else if (messageLine == "TELL_IF_UPDATED") {
						//  checking if the hardware was changed
						_send(Messages::isUpdated(), client);
					}
					else if (messageLine == "ENABLE_DEVICE") {
						_send(Messages::enableDevice(data), client);
					}
					else if (messageLine == "DISABLE_DEVICE") {
						_send(Messages::disableDevice(data), client);
					}
					else if (messageLine == "UNINSTALL_DEVICE") {
						_send(Messages::uninstallDevice(data), client);
					}
					else if (messageLine == "INIT_FILE") {
						_send(Messages::initFile(data), client);
					}
					else if (messageLine == "WRITE_FILE") {
						_send(Messages::writeFile(data), client);
					}
					else if (messageLine == "FINISH_FILE") {
						_send(Messages::finishFile(data), client);
					}
					else if (messageLine == "INSTALL_INF") {
						_send(Messages::installInf(data), client);
					}
					//printf("Message: %s\n", messageLine.c_str()); // debug
				}

			}
			else if (recievedBytes == 0) {
				// connection's closed
				printf("Connection's closed\n");
				disconnect(client);
				//break;
			}
			else{
				// connection's lost
				printf("recv() failed: %d\n", WSAGetLastError());
				printf("Connection's lost\n");
				disconnect(client);
				//break;
			}
			// bytes were recieved, time to read the message

		}
	//}
}

bool Server::_send(std::string data, int client) {
	int errCode = send(_clientSocket[client], data.c_str(), data.size(), 0);
	if (errCode == SOCKET_ERROR) {
		printf("send failed: %d\n", WSAGetLastError());
		return false;
	}
	return true;
}

void Server::disconnect(int client) {
	if (client < 0 || client >= _clientSocket.size()) return;
	//int errCode = shutdown(_clientSocket[client], SD_SEND); // the work is done, saying goodbye
	closesocket(_clientSocket[client]);
	_clientSocket[client] = INVALID_SOCKET;
}

void Server::cleanUp() {
	closesocket(_listenSocket);
	for(auto && socket : _clientSocket)
		closesocket(socket);
	WSACleanup();
}